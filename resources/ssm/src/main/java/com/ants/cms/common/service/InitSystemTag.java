package com.ants.cms.common.service;

import com.ants.cms.common.cover.AnnotationGenerator;
import freemarker.template.TemplateDirectiveModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;


/**
 * Created by liushun on 2016/11/7.
 */
@Service
public abstract class InitSystemTag implements TemplateDirectiveModel, InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Override
    public void afterPropertiesSet() throws Exception {
        String className = this.getClass().getName();
        String tagName = className.substring(className.lastIndexOf(".") + 1, className.length()-3).toLowerCase();
        //除开父类其他都生成标签
        if(!"initsystem".equals(tagName)) {
            freeMarkerConfigurer.getConfiguration().setSharedVariable(tagName, applicationContext.getBean(this.getClass()));
            logger.debug("系统生成创建了 @{} 标签，可以到模板页面使用，标签使用详情请看工程目录下面的README.md", tagName);
        }
    }

}
