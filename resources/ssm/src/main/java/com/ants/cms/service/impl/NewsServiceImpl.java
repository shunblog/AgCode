package com.ants.cms.service.impl;

import com.ants.cms.common.service.BaseServiceImpl;
import com.ants.cms.mapper.NewsMapper;
import com.ants.cms.mapper.ProductMapper;
import com.ants.cms.service.NewsService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by liushun on 2016/10/28.
 */
@Service
public class NewsServiceImpl extends BaseServiceImpl implements NewsService, InitializingBean{

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public void afterPropertiesSet() throws Exception {
        super.setBaseMapper(newsMapper);
    }
}
