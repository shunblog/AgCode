package com.ants.cms.common.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;

/**
 * @null 验证对象是否为空
 * @notnull 验证对象是否为非空
 * @asserttrue 验证 boolean 对象是否为 true
 * @assertfalse 验证 boolean 对象是否为 false
 * @min 验证 number 和 string 对象是否大等于指定的值
 * @max 验证 number 和 string 对象是否小等于指定的值
 * @decimalmin 验证 number 和 string 对象是否大等于指定的值，小数存在精度
 * @decimalmax 验证 number 和 string 对象是否小等于指定的值，小数存在精度
 * @size 验证对象（array,collection,map,string）长度是否在给定的范围之内
 * @digits 验证 number 和 string 的构成是否合法
 * @past 验证 date 和 calendar 对象是否在当前时间之前
 * @future 验证 date 和 calendar 对象是否在当前时间之后
 * @pattern 验证 string 对象是否符合正则表达式的规则
 * 简单封装 SpringMVC验证框架Validation
 * Created by Administrator on 2016/7/31.
 */
public class SpringValidators {

    /**
     * 数据校验，无错误时返回null
     *
     * @param result
     * @return
     */
    public static String validate(BindingResult result) {
        StringBuffer sb = new StringBuffer();
        if (result.hasErrors()) {
            List<FieldError> allErrors = result.getFieldErrors();
            for (FieldError error : allErrors)
                sb.append(error.getField() + " " + error.getDefaultMessage() + ";");
            return sb.toString();
        } else
            return null;
    }
}