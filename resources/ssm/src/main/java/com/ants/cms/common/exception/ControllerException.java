package com.ants.cms.common.exception;

import com.ants.cms.common.enums.ResponseCode;
import com.ants.cms.common.utils.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * 自定义Controller增强型异常处理
 * 可以根据不同的异常类型返回详细的异常结果
 * Created by liushun on 2016/7/29.
 */
@ControllerAdvice
public class ControllerException {

    private static final Logger logger = LoggerFactory.getLogger(ControllerException.class);

    @InitBinder
    public void initBinder(WebDataBinder binder) {

    }

    //运行时异常
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public Object runtimeExceptionHandler(RuntimeException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.SYSTEM_ERROR, ex.getMessage());
    }

    //空指针异常
    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public Object nullPointerExceptionHandler(NullPointerException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.NULL_ERROR, ex.getMessage());
    }

    //类型转换异常
    @ExceptionHandler(ClassCastException.class)
    @ResponseBody
    public Object classCastExceptionHandler(ClassCastException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.CONVERT_ERROR, ex.getMessage());
    }

    //IO异常
    @ExceptionHandler(IOException.class)
    @ResponseBody
    public Object iOExceptionHandler(IOException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.IO_ERROR, ex.getMessage());
    }

    //未知方法异常
    @ExceptionHandler(NoSuchMethodException.class)
    @ResponseBody
    public Object noSuchMethodExceptionHandler(NoSuchMethodException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.METHOD_ERROR, ex.getMessage());
    }

    //数组越界异常
    @ExceptionHandler(IndexOutOfBoundsException.class)
    @ResponseBody
    public Object indexOutOfBoundsExceptionHandler(IndexOutOfBoundsException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.ARRAY_ERROR, ex.getMessage());
    }

    //400错误
    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseBody
    public Object requestNotReadable(HttpMessageNotReadableException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.PARSE_ERROR, ex.getMessage());
    }

    //400错误
    @ExceptionHandler({TypeMismatchException.class})
    @ResponseBody
    public Object requestTypeMismatch(TypeMismatchException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.MATH_ERROR, ex.getMessage());
    }

    //400错误
    @ExceptionHandler({MissingServletRequestParameterException.class})
    @ResponseBody
    public Object requestMissingServletRequest(MissingServletRequestParameterException ex) {
        logger.error(ex.getMessage());
        return ResponseUtil.jsonException(ResponseCode.BIND_ERROR, ex.getMessage());
    }
    //405错误

    //406错误
    @ExceptionHandler({HttpMediaTypeNotAcceptableException.class})
    @ResponseBody
    public Object request406() {
        return ResponseUtil.jsonException(ResponseCode.MIDEA_TYPE_ERROR);
    }

    //500错误
    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    @ResponseBody
    public Object server500(RuntimeException ex) {
        logger.error(ex.getMessage());
        ex.printStackTrace();
        return ResponseUtil.jsonException(ResponseCode.SYSTEM_ERROR, ex.getMessage());
    }


}
