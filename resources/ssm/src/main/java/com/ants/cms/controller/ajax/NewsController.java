package com.ants.cms.controller.ajax;

import com.ants.cms.common.bean.Page;
import com.ants.cms.common.utils.ResponseUtil;
import com.ants.cms.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/9.
 */
@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * 分页
     * @return
     */
    @RequestMapping(value = "/getNewsPage", method = RequestMethod.POST)
    public Object getNewsPage(){
        List<Map> result = newsService.queryList(null);
        return result;
    }

    @RequestMapping(value = "/getNewsPage0", method = RequestMethod.POST)
    public Object getNewsPage(@RequestParam Integer pageCurrent
            , @RequestParam Integer pageSize) {
        //MapUtil map = MapUtil.newHashMap().set("orderField", orderField).set("orderDirection", orderDirection);
        Page page = newsService.queryPage(pageCurrent, pageSize);
        return ResponseUtil.jsonSuccess(page);
    }



}
