package com.ants.cms.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 容器启动、销毁 ,Session添加、移除、销毁, Request添加、移除、销毁
 * @author liushun
 * @version 1.0
 * @Date 2015-12-29
 */
@WebListener
public class WebContextListener implements ServletContextListener{

    private final  Logger logger  =  LoggerFactory.getLogger(WebContextListener.class);

    /**
     * 服务容器启动初始化加加载日志文件
     * @param servletContextEvent
     */
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.debug(">>> Web :: 容器初始化完成 Success!");
        logger.info("\n"+
        "  ┏┓      ┏┓\n" +
                "┏┛┻━━━┛┻┓                                       1.技术只是解决问题的选择，而不是解决问题的根本\n" +
                "┃              ┃\n" +
                "┃      ━      ┃                                       2.聪明是代码清晰的敌人\n" +
                "┃  ┳┛  ┗┳  ┃\n" +
                "┃              ┃                                       3.写尽可能少的代码\n" +
                "┃      ┻      ┃\n" +
                "┃              ┃                                       4.注释是代码表述的最后选择\n" +
                "┗━┓      ┏━┛\n" +
                "    ┃      ┃                                           5.在编写代码之前你应当清楚你的代码要做什么\n" +
                "    ┃ Ants ┃\n" +
                "    ┃      ┗━━━┓                                   6.每天都要学一些新东西\n" +
                "    ┃ Start Success┣┓\n" +
                "    ┃              ┏┛                                 7.写代码应该成为一种乐趣\n" +
                "    ┗┓┓┏━┳┓┏┛\n" +
                "      ┃┫┫  ┃┫┫                                     8.你不需要无所不知\n" +
                "      ┗┻┛  ┗┻┛");
    }

    /**
     * 服务容器销毁
     * @param servletContextEvent
     */
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.debug(">>> Web :: 容器销毁 Success !");
    }

}