package com.ants.cms.service;

import com.ants.cms.common.service.BaseService;

/**
 * 新闻相关业务待扩展，目前基本都是单表操作，
 * BaseService 基本满足
 * Created by liushun on 2016/10/28.
 */
public interface NewsService extends BaseService {

}
