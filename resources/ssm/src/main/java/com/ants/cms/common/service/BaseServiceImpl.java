package com.ants.cms.common.service;

import com.ants.cms.common.bean.Page;
import com.ants.cms.common.db.BaseMapper;
import com.github.pagehelper.PageHelper;
import com.sun.xml.internal.bind.v2.model.core.ID;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by liushun on 2016/10/28.
 */
public class BaseServiceImpl implements BaseService {

    private BaseMapper baseMapper;

    public void setBaseMapper(BaseMapper baseMapper) {
        this.baseMapper = baseMapper;
    }

    @Override
    public List queryList(Object conditions) {
        return baseMapper.selectList(conditions);
    }

    @Override
    public int queryCount(Object conditions) {
        return baseMapper.selectCount(conditions);
    }

    @Override
    public Object queryById(Serializable serializable) {
        return baseMapper.selectById(serializable);
    }

    @Override
    public int insert(Object conditions) {
        return baseMapper.insert(conditions);
    }

    @Override
    public int delete(Object conditions) {
        return baseMapper.delete(conditions);
    }

    @Override
    public int deleteById(Serializable serializable) {
        return baseMapper.deleteById(serializable);
    }

    @Override
    public int deleteByIds(List ids) {
        int result = 0;
        for (Object id: ids) {
            baseMapper.deleteById((Serializable) id);
            result ++;
        }
        return result;
    }

    @Override
    public int update(Object conditions) {
        return baseMapper.update(conditions);
    }

    @Override
    public int updateById(Serializable id) {
        return baseMapper.updateById(id);
    }

    @Override
    public Page queryPage(Integer pageCurrent, Integer pageSize) {
        return queryPage(pageCurrent, pageSize, null);
    }

    @Override
    public Page queryPage(Integer pageCurrent, Integer pageSize, Map conditions) {
        PageHelper.startPage(pageCurrent, pageSize);
        List list = baseMapper.selectList(conditions);
        return new Page(list);
    }


}
