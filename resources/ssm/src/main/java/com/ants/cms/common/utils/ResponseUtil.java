package com.ants.cms.common.utils;


import com.ants.cms.common.bean.Page;
import com.ants.cms.common.enums.ResponseCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liushun
 * @version 1.0
 * @Date 2016-03-09
 */
public class ResponseUtil {


    //执行状态
    public static final String STATE = "ResponseCode";

    //消息描述
    public static final String MSG = "ResponseMessage";

    //数据对象
    public static final String RESULT = "Data";

    //时间戳
    private static final String TIMESPAN = "TimeSpan";

    //异常消息
    private static final String ERROR = "Exception";

    //当前页数
    private static final String PAGECURRENT = "PageCurrent";

    //每页大小
    private static final String PAGESIZE = "PageSize";

    //总记录数
    private static final String TOTALCOUNT = "TotalCount";

    //总页数
    private static final String TOTALPAGE = "TotalPage";


    /**
     * 操作成功
     *
     * @param data      数据对象
     * @param startTime 开始时间
     * @param <T>
     * @return
     */
    public static <T> Map jsonSuccess(T data, Long startTime) {
        Map map = new HashMap();
        map.put(STATE, ResponseCode.SUCCESS.getCode());
        map.put(MSG, ResponseCode.SUCCESS.getMsg());
        if (startTime != null)
            map.put(TIMESPAN, System.currentTimeMillis() - startTime + " msec");
        if (data instanceof Page) {
            Page page = ((Page) data);
            map.put(RESULT, page.getData());
            map.put(PAGECURRENT, page.getPageCurrent());
            map.put(PAGESIZE, page.getPageSize());
            map.put(TOTALCOUNT, page.getTotalCount());
            map.put(TOTALPAGE, page.getTotalPage());
        } else {
            map.put(RESULT, data);
        }
        return map;
    }

    public static <T> Map jsonSuccess(T data) {
        return jsonSuccess(data, null);
    }

    /**
     * 操作异常失败
     *
     * @param responseCode 枚举响应码
     * @param exception    异常信息
     * @return
     */
    public static Map jsonException(ResponseCode responseCode, String exception) {
        Map map = new HashMap();
        map.put(STATE, responseCode.getCode());
        map.put(MSG, responseCode.getMsg());
        map.put(ERROR, exception);
        return map;
    }

    public static Map jsonException(ResponseCode responseCode) {
        Map map = new HashMap();
        map.put(STATE, responseCode.getCode());
        map.put(MSG, responseCode.getMsg());
        return map;
    }

    /**
     * 操作错误
     *
     * @param responseCode 枚举响应码
     * @return
     */
    public static Map jsonFail(ResponseCode responseCode) {
        Map map = new HashMap();
        map.put(STATE, responseCode.getCode());
        map.put(MSG, responseCode.getMsg());
        return map;
    }

    /**
     * 操作错误自定义错误码
     *
     * @param msg 自定义系统错误消息
     * @return
     */
    public static Map jsonFail(String msg) {
        Map map = new HashMap();
        map.put(STATE, ResponseCode.SYSTEM_ERROR.getCode());
        map.put(MSG, msg);
        return map;
    }

    /**
     * easyui使用datagrid返回数据格式
     * @param row
     * @param total
     * @return
     */
    public static Map easyuiResponse(List row, long total){
        Map<String, Object> map = new HashMap();
        map.put("rows", row);
        map.put("total", total);
        map.put(STATE, ResponseCode.SUCCESS.getCode());
        map.put(MSG, ResponseCode.SUCCESS.getMsg());
        return map;
    }
}