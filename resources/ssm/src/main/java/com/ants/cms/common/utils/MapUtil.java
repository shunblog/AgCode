package com.ants.cms.common.utils;

import java.util.HashMap;
import java.util.Map;

/**+
 * 为了处理mybatis字符串为空或null不输出字段名称
 * 在mybatis配置文件中 resultType使用
 * Created by liushun on 2016/8/22.
 */
public class MapUtil extends HashMap{

    private static MapUtil map = null;

    @Override
    public Object put(Object key, Object value) {
        if(value == null)
            return super.put(key, "");
        return super.put(key, value);
    }

    public static synchronized MapUtil newHashMap(){
        return map = new MapUtil();
    }

    public MapUtil set(Object key, Object value){
        map.put(key, value);
        return map;
    }

}
