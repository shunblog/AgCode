package com.ants.cms.mapper;

import com.ants.cms.common.annotation.MyBatisMapper;
import com.ants.cms.common.db.BaseMapper;

/**
 * Created by liushun on 2016/10/28.
 */
@MyBatisMapper
public interface ProductMapper extends BaseMapper {

}
