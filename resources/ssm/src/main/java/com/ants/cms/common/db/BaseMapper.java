package com.ants.cms.common.db;

import com.ants.cms.common.annotation.MyBatisMapper;
import com.sun.xml.internal.bind.v2.model.core.ID;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 公共的通用Mybatis接口
 * Created by liushun on 2016/10/28.
 */
public interface BaseMapper<T,ID extends Serializable> {


    /**
     * 查询数据列表
     * @param conditions 条件全部使用=号and条件
     * @return
     */
    List<T> selectList(T conditions);


    /**
     * 根据实体类不为null的字段查询总数
     * @param conditions 条件全部使用=号and条件
     * @return
     */
    int selectCount(T conditions);

    /**
     * 根据主键查询一条记录
     * @param id 主键
     * @return
     */
    T selectById(ID id);


    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @param conditions 条件集合
     * @return
     */
    int insert(T conditions);


    /**
     * 根据实体类中字段不为null的条件进行删除
     * @param conditions 条件集合
     * @return
     */
    int delete(T conditions);

    /**
     * 根据主键ID删除
     * @param id 主键
     * @return
     */
    int deleteById(ID id);



    /**
     * 根据多条件进行更新,只会更新不是null的数据
     * @param conditions
     * @return
     */
    int update(T conditions);


    /**
     * 根据主键进行更新,只会更新不是null的数据
     * @param id 主键
     * @return
     */
    int updateById(ID id);
}
