package com.ants.cms.tags;


import com.ants.cms.common.service.InitSystemTag;
import com.ants.cms.service.NewsService;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import static freemarker.template.ObjectWrapper.DEFAULT_WRAPPER;

/**
 * 新闻列表标签
 * Created by liushun on 2016/10/27.
 */
@Service
public class NewsTag extends InitSystemTag {

    @Autowired
    private NewsService newsService;

    @Override
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {

        List<Map> list = newsService.queryList(null);

        env.setVariable("newslist", DEFAULT_WRAPPER.wrap(list));

        if (body != null) {
            env.getOut().write("<h2>标题</h2>");
            body.render(env.getOut());
            env.getOut().write("<h2>结尾</h2>");
        } else {
            throw new RuntimeException("missing body");
        }
    }

}
