package com.ants.cms.controller.web;

import freemarker.template.TemplateException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * 基本路径匹配类
 *
 * 由于现在网页为了效果基本上都是先加载页面然后利用ajax加载数据
 * 所以可以直接利用通配符匹配网页路径，省去了一个路径对应一个模板的繁琐
 * Created by liushun on 2016/8/11.
 */
@Controller
public class PageJumpController {

    private static String SKIN = "default";

    /**
     * 匹配views目录下面一级路径
     * @param page 页面
     * @return
     */
    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String page(@PathVariable String page) {
        return SKIN+"/"+page;
    }


    /**
     * 匹配views 目录下面的二级路径
     * @param folder 文件夹名称
     * @param page 页面
     * @return
     */
    @RequestMapping(value = "/{folder}/{page}", method = RequestMethod.GET)
    public String project_page(@PathVariable String folder, @PathVariable String page) {
        return SKIN+ "/" + folder + "/" + page;
    }


    @RequestMapping(value = "/skin/{skin}", method = RequestMethod.GET)
    @ResponseBody
    public String tz(@PathVariable String skin)  {
        this.SKIN = skin;
        return skin +" skin set success!";
    }
}
