package com.ants.cms.entity;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/1.
 */
public class News implements Serializable{

    private String Id;

    private String title;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
