package com.ants.cms.common.enums;

/**
 * 响应码定义
 * Created by liushun on 2016/7/15.
 */
public enum ResponseCode {

    SUCCESS(0, "成功"),

    TOKEN_INFO(100, "无效的Token"),

    AUTH_INFO(101, "无操作权限"),

    PARAM_INFO(102, "缺少参数或值为空"),

    PARAM_ERROR(103, "参数不合法"),

    LOGIN_INFO(104, "请重新登录"),

    MIDEA_TYPE_ERROR(105, "浏览器无法解析服务端媒体类型"),

    SYSTEM_ERROR(1000, "[服务器]运行时异常，请联系系统管理员"),

    NULL_ERROR(1001, "[服务器]空值异常"),

    CONVERT_ERROR(1002, "[服务器]数据类型转换异常"),

    IO_ERROR(1003, "[服务器]IO异常"),

    METHOD_ERROR(1004, "[服务器]未知方法异常"),

    ARRAY_ERROR(1005, "[服务器]数组越界异常"),

    NOWEB_ERROR(1006, "[服务器]网络异常"),

    PARSE_ERROR(1007, "[服务器]数据解析异常"),

    BIND_ERROR(1009, "[服务器]数据绑定异常"),

    MATH_ERROR(1009, "[服务器]数据类型匹配异常");

    private int code;
    private String msg;

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
