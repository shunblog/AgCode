package com.ants.cms.common.plugins;

/**
 * 所有插件都需要实现这两个个接口
 * Created by liushun on 2016/11/7.
 */
public interface Plugin {

    //初始化开启插件
    boolean start();

    //销毁插件
    boolean destroy();
}
