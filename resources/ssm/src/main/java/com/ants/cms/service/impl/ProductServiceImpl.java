package com.ants.cms.service.impl;

import com.ants.cms.common.db.BaseMapper;
import com.ants.cms.common.service.BaseServiceImpl;
import com.ants.cms.mapper.ProductMapper;
import com.ants.cms.service.ProductService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * Created by liushun on 2016/10/28.
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl implements ProductService, InitializingBean {

    @Autowired
    private ProductMapper productMapper;


    @Override
    public void afterPropertiesSet() throws Exception {
        super.setBaseMapper(productMapper);
    }
}
