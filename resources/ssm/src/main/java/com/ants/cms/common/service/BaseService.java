package com.ants.cms.common.service;

import com.ants.cms.common.bean.Page;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 最基本的业务方法
 * 通用service接口设计
 * Created by liushun on 2016/10/27.
 */
public interface BaseService<T,ID extends Serializable> {

    /**
     * 查询数据列表
     * @param conditions 条件全部使用=号and条件
     * @return
     */
    List<T> queryList(T conditions);


    /**
     * 根据实体类不为null的字段查询总数
     * @param conditions 条件全部使用=号and条件
     * @return
     */
    int queryCount(T conditions);

    /**
     * 根据主键查询一条记录
     * @param id 主键
     * @return
     */
    T queryById(ID id);



    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @param conditions 条件集合
     * @return
     */
    int insert(T conditions);


    /**
     * 根据实体类中字段不为null的条件进行删除
     * @param conditions 条件集合
     * @return
     */
    int delete(T conditions);

    /**
     * 根据主键ID删除
     * @param id 主键
     * @return
     */
    int deleteById(ID id);

    /**
     * 批量删除
     * @param ids id集合
     * @return
     */
    int deleteByIds(List<Serializable> ids);


    /**
     * 根据多条件进行更新,只会更新不是null的数据
     * @param conditions
     * @return
     */
    int update(T conditions);


    /**
     * 根据主键进行更新,只会更新不是null的数据
     * @param id 主键
     * @return
     */
    int updateById(ID id);

    /**
     * 基本分页
     * @param pageCurrent 当前页数
     * @param pageSize 每页大小
     * @return
     */
    Page queryPage(Integer pageCurrent, Integer pageSize);

    /**
     * 高级分页
     * @param pageCurrent 当前页数
     * @param pageSize 每页大小
     * @param conditions 字段条件
     * @return
     */
    Page queryPage(Integer pageCurrent, Integer pageSize, Map conditions);
}
