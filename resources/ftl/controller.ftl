${package!''}import com.alibaba.fastjson.JSON;
import com.ants.cms.common.utils.ResponseUtil;
import com.ants.cms.common.utils.SpringValidators;
import com.ants.cms.entity.${className};
import com.ants.cms.service.${className}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
*
* @author ${author!''}
* @version ${version!''} ${dateTime!''}
*/
@RestController
@RequestMapping("/${tableName}")
public class ${className}Ajax {

    @Autowired
    private ${className}Service ${tableName}Service;

    /**
    * 查询列表分页
    * @param pageCurrent 当前页数
    * @param pageSize 每页大小
    * @param filter 过滤字段 集合
    * @return
    */
    @RequestMapping(value = "/get${className}List", method = RequestMethod.GET)
    public Object get${className}List(@RequestParam Integer pageCurrent
            , @RequestParam Integer pageSize
            , String filter){
        Map conditions = null;
        if("".equals(filter))
            conditions = JSON.parseObject(filter, Map.class);
        return ResponseUtil.jsonSuccess(${tableName}Service.queryPage(pageCurrent, pageSize, conditions));
    }


    /**
    * 新增一条数据
    * @param ${tableName} 实体
    * @param result 校验
    * @return
    */
    @RequestMapping(value = "/save${className}", method = RequestMethod.POST)
    public Object save${className}(${className} ${tableName}, BindingResult result) {
        String errMsg = SpringValidators.validate(result);
        if (errMsg!=null) return ResponseUtil.jsonFail(errMsg);
        return ResponseUtil.jsonSuccess(${tableName}Service.insert(${tableName}));
    }


    /**
    * 根据主键ID删除数据
    * @param ids 主键ID数组
    * @return
    */
    @RequestMapping(value = "/delete${className}", method = RequestMethod.POST)
    public Object delete${className}(@RequestParam("ids[]") List<Serializable> ids) {
        return ResponseUtil.jsonSuccess(${tableName}Service.deleteByIds(ids));
    }

    /**
    * 修改提交的实体数据
    * @param ${tableName} 实体
    * @param result 校验
    * @return
    */
    @RequestMapping(value = "/modify${className}", method = RequestMethod.POST)
    public Object modify${className}(${className} ${tableName}, BindingResult result) {
        String errMsg = SpringValidators.validate(result);
        if (errMsg!=null) return ResponseUtil.jsonFail(errMsg);
        return ResponseUtil.jsonSuccess(${tableName}Service.update(${tableName}));
    }

    /**
    * 根据主键ID查询单条数据
    * @param id 主键ID
    * @return
    */
    @RequestMapping(value = "/get${className}", method = RequestMethod.GET)
    public Object get${className}(@RequestParam Integer id) {
        return ResponseUtil.jsonSuccess(${tableName}Service.queryById(id));
    }

}
