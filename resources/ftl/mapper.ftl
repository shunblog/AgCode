${package[0]!''}import com.ants.cms.common.annotation.MyBatisMapper;
import com.ants.cms.common.db.BaseMapper;


/**
* @author ${author!''}
* @version ${version!''} ${dateTime!''}
*/
@MyBatisMapper
public interface ${className}Mapper extends BaseMapper {

}