${package[0]!''}<#if isExtends??>import com.ants.cms.common.service.BaseService;</#if>

/**
* ${tableDesc!''}待扩展
*
* @author ${author!''}
* @version ${version!''} ${dateTime!''}
*/
public interface ${className}Service<#if isExtends??> extends BaseService</#if> {

}