<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
${tableDesc}<mapper namespace="${package[1]!''}mapper.${className}Mapper" >

    <!--数据和实体的对应映射-->
    <sql id="Base_Column_List" >
        <#list properties as prop>
        ${prop.sqlColumn} as ${prop.columnName}<#if prop_has_next>, </#if>
        </#list>
    </sql>

    <!-- //TODO 多条件查询,自己扩充条件 -->
    <select id="selectList" resultType="${package[1]!''}entity.${className}">
        select <include refid="Base_Column_List"/> from ${tableName}
    </select>

    <!-- //TODO 多条件删除,自己扩充条件-->
    <delete id="delete">

    </delete>

    <!-- //TODO 多条件更新,自己扩充条件-->
    <update id="update">

    </update>

    <!-- //TODO selectCount 多条件数据统计-->

    <!-- 根据主键ID查询一条记录-->
    <select id="selectById" resultType="${package[1]!''}entity.${className}">
        select <include refid="Base_Column_List"/> from ${tableName} where id = ${r'#{id}'}
    </select>

    <!-- 插入不为空的字段 -->
    <insert id="insert">
        insert into ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides="," >
            <#list properties as prop>
            <if test="${prop.columnName} != null" >
                ${prop.sqlColumn},
            </if>
            </#list>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides="," >
            <#list properties as prop>
            <if test="${prop.columnName} != null" >
                ${r'#{'}${prop.columnName}${r'},'}
            </if>
            </#list>
        </trim>
    </insert>

    <!-- 根据主键Id删除数据-->
    <delete id="deleteById">
        delete from ${tableName} where id = ${r'#{id}'}
    </delete>

    <!-- 据主键Id修改字段不为空的数据 -->
    <update id="updateById">
        update ${tableName}
        <set >
            <#list properties as prop>
            <if test="${prop.columnName} != null" >
            ${prop.sqlColumn} = ${r'#{'}${prop.columnName}${r'},'}
            </if>
            </#list>
        </set>
        where id = ${r'#{id}'}
    </update>
</mapper>