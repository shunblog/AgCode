${package!''}import java.io.Serializable;

/**
* ${tableDesc!''}(${tableName!''})
*
* @author ${author!''}
* @version ${version!''} ${dateTime!''}
*/
${jpaEntity!''}${jpaTable!''}public class ${class} implements Serializable {

<#list properties as prop>
    /** ${prop.columnDescription} */
    <#if prop_index == 0>${jpaId!''}</#if><#if jpaColumn??>${prop.sqlColumnName}</#if>private ${prop.columnType} ${prop.columnName};

</#list>

<#list properties as prop>

    public ${prop.columnType} get${prop.columnCapital}(){
        return ${prop.columnName};
    }

    public void set${prop.columnCapital}(${prop.columnType} ${prop.columnName}){
        this.${prop.columnName} = ${prop.columnName};
    }

</#list>


}