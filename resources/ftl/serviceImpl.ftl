${package[1]!''}<#if isExtends??>import com.ants.cms.common.service.BaseServiceImpl;</#if>
import com.ants.cms.mapper.${className}Mapper;
import com.ants.cms.service.${className}Service;
<#if isExtends??>import org.springframework.beans.factory.InitializingBean;</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author ${author!''}
* @version ${version!''} ${dateTime!''}
*/
@Service
public class ${className}ServiceImpl <#if isExtends??>extends BaseServiceImpl </#if>implements ${className}Service<#if isExtends??>, InitializingBean</#if>{

    @Autowired
    private ${className}Mapper ${tableName}Mapper;

    <#if isExtends??>
    @Override
    public void afterPropertiesSet() throws Exception {
        super.setBaseMapper(${tableName}Mapper);
    }
    </#if>
}
