#AgCode

=== 运行环境需要安装jdk1.7.0 以上和JavaRuntime 可以自行用jar2exe 打包 ===

1：AgCode是用JavaSwing开发

2：一键生成项目是构建简单的ssm框架, 只是复制一套基本的框架结构

3：代码生成部分主要采用Freemarker来实现, 目前只做了针对Mysql的生成

4：工具含有生成Entity层, Mapper层, Service层, Controller层, 目前Entity、Controller、SqlXml支持自定义模板生成, 也可以自己修改ftl目录下面的模板文件, 后期有时间会补上其他类型数据生成

5：config配置文件部分参数支持自定义设置, 需要还原默认配置时只需要删除软件下面的config文件和ftl目录.

------------------------
=== 软件截图 ===
![image](http://git.oschina.net/shunblog/AgCode/raw/master/demo_1.png)
#
![image](http://git.oschina.net/shunblog/AgCode/raw/master/demo_2.png)
#
![image](http://git.oschina.net/shunblog/AgCode/raw/master/demo_3.png)
------------------------

* 作者：liushun

* 邮箱：358113099@qq.com

* 版本：v1.0

* 日期：2016.12.1

