package utils;

import ui.AgCodeMain;

import java.awt.*;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by liushun on 2016/11/29.
 */
public class CamelCaseUtils {

    private static final char SEPARATOR = '_';

    public static String toUnderlineName(String s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            boolean nextUpperCase = true;

            if (i < (s.length() - 1)) {
                nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
            }

            if ((i >= 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    if (i > 0) sb.append(SEPARATOR);
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }

        s = s.toLowerCase();

        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    private static String toCapitalizeCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String deletePrefix(String prefixs, String s){
        try {
            for (String prefix : prefixs.split(",")) {
                int i = s.toUpperCase().indexOf(prefix.toUpperCase());
                if (i != -1) {
                    return s.substring(i + prefix.length(), s.length());
                }
            }
            return s;
        }catch (Exception e){
            return s;
        }
    }

    public static String toCapital(String s, boolean isFrist){
        if (s == null) {
            return null;
        }
        int i = s.indexOf(SEPARATOR);
        int index = 0;
        while (s.indexOf(SEPARATOR)!=-1) {
            index = s.indexOf(SEPARATOR);
            if(index!=0&&index!=s.length()) {
                s= s.substring(0, index)+s.substring(index+1, index+2).toUpperCase()+s.substring(index+2, s.length());
            }else{
                return null;
            }
        }
        char[] cs=s.toCharArray();
        if(isFrist)cs[0]-=32;
        return String.valueOf(cs);
    }


    public static String frist2Capitals(String str) {
        char[] cs = str.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }

    public static String sqlType2JavaType(String sqlType) {
        if(Arrays.asList(getType("Boolean")).contains(sqlType)){
            return "Boolean";
        }else if(Arrays.asList(getType("Integer")).contains(sqlType)){
            return "Integer";
        }else if(Arrays.asList(getType("Long")).contains(sqlType)){
            return "Long";
        }else if(Arrays.asList(getType("Float")).contains(sqlType)){
            return "Float";
        }else if(Arrays.asList(getType("Double")).contains(sqlType)){
            return "Double";
        }else if(Arrays.asList(getType("String")).contains(sqlType)){
            return "String";
        }else if(Arrays.asList(getType("Date")).contains(sqlType)){
            return "java.sql.Date";
        }else if(Arrays.asList(getType("Timestamp")).contains(sqlType)){
            return "java.sql.Timestamp";
        }else if(Arrays.asList(getType("Blod")).contains(sqlType)){
            return "Blod";
        }

        return null;
    }


    private static String[] getType(String type){
        try {
            String property = AgCodeMain.config.getProperty("conver." + type);
            property = (property == null ? "" : property);
            String[] sz = property.split("\\|");
            return sz;
        }catch (Exception e){
            AgCodeMain.log("配置文件数据库类型配置错误!", Color.red, true);
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(CamelCaseUtils.toUnderlineName("ISOCertifiedStaff"));
        System.out.println(CamelCaseUtils.toUnderlineName("CertifiedStaff"));
        System.out.println(CamelCaseUtils.toUnderlineName("iso_certified_staff"));
        System.out.println(CamelCaseUtils.toCamelCase("iso_certified_staff"));
        System.out.println(CamelCaseUtils.toCamelCase("certifiedStaff"));
        System.out.println(CamelCaseUtils.deletePrefix("Cs_", "cs_user_id"));
    }
}