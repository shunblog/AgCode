package utils;

import entity.Property;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by liushun on 2016/11/22.
 */
public class FileUtil {

    /**
     * 读取指定路径文本文件
     *
     * @param filePath 文件路径
     * @return
     */
    public static String read(String filePath) {
        StringBuilder str = new StringBuilder();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filePath));
            String s;
            try {
                while ((s = in.readLine()) != null)
                    str.append(s + '\n');
            } finally {
                in.close();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str.toString();
    }

    /**
     * 根据流来读取文件内容
     * @param inputStream
     * @return
     */
    public static String readInputStream(InputStream inputStream) {
        StringBuilder str = new StringBuilder();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String s;
            try {
                while ((s = in.readLine()) != null)
                    str.append(s + '\n');
            } finally {
                in.close();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str.toString();
    }

    /**
     * 写入指定的文本文件，append为true表示追加，false表示重头开始写，
     * text是要写入的文本字符串，text为null时直接返回
     *
     * @param filePath 文件路径
     * @param append   是否追加
     * @param text     文本内容
     */
    public static void write(String filePath, boolean append, String text) {
        if (text == null)
            return;
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath, append));
            try {
                out.write(text);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 文件复制
     * @param in 输入流
     * @param dest 目标文件
     */
    public static void copyFile(InputStream in, String dest) {
        FileOutputStream out = null;
        try {
            File file = new File(dest);
            String s = String.valueOf(dest.toCharArray()[dest.length() - 1]);
            if("/".equals(s)||"\\".equals(s))
                file.mkdirs();
            else{
                file.getParentFile().mkdirs();
                file.createNewFile();
                out = new FileOutputStream(file);
                int c;
                byte buffer[] = new byte[1024];
                while ((c = in.read(buffer)) != -1) {
                    for (int i = 0; i < c; i++)
                        out.write(buffer[i]);
                }
                in.close();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            try {
                 if(out!=null) out.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void fileContentModify(String filePath, Properties prop) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
            out.write("#配置数据库连接属性#\n");
            out.write("db.type=mysql\n");
            out.write("db.ip=" + prop.getProperty("db.ip") + "\n");
            out.write("db.name=" + prop.getProperty("db.name") + "\n");
            out.write("db.port=" + prop.getProperty("db.port") + "\n");
            out.write("db.username=" + prop.getProperty("db.username") + "\n");
            out.write("db.password=" + prop.getProperty("db.password") + "\n\n\n");

            out.write("#配置生成参数#\n");
            out.write("package.packagePath=" + prop.getProperty("package.packagePath") + "\n");
            out.write("package.tableNames=" + prop.getProperty("package.tableNames") + "\n");
            out.write("package.tablePrefix=" + prop.getProperty("package.tablePrefix") + "\n");
            out.write("package.ftlPath=" + prop.getProperty("package.ftlPath") + "\n\n\n");


            out.write("#配置数据库转换属性#\n");
            out.write("conver.String=varchar|text|nvarchar|char|nchar\n");
            out.write("conver.Integer=int|tinyint|smallint\n");
            out.write("conver.Boolean=bit\n");
            out.write("conver.Long=bigint\n");
            out.write("conver.Float=float\n");
            out.write("conver.Double=decimal|real|numeric|money|smallmoney\n");
            out.write("conver.Date=datetime|date\n");
            out.write("conver.Timestamp=timestamp\n");
            out.write("conver.Clob=\n");
            out.write("conver.Blod=image\n\n\n");

            out.write("#作者已经版本#\n");
            out.write("java.author=liushun\n");
            out.write("java.version=1.0.0\n");
            out.close();
            System.out.println("config 配置文件写入成功");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
