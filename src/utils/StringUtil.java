package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by liushun on 2016/11/18.
 */
public class StringUtil {

    /**
     * 截取字符串
     * @param str 需要截取的字符串
     * @param s 截取标记
     * @param count 截取标记次数
     * @return
     */
    public static String subLastString(String str,String s, int count){
        if(!"/".equals(String.valueOf(str.charAt(str.length()-1))))
            count = count -1;
        for(int i = 0;i < count; i++){
            str = str.substring(0, str.lastIndexOf(s));
        }
        return str;
    }

    /**
     * 在数组中插入字符返回字符串
     * @param list 数组
     * @param str 字符
     * @return
     */
    public static String join(List<String> list, String str){
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i<list.size() ;i++){
            sb.append(list.get(i));
            if(i!=list.size()-1)
                sb.append(str);
        }
        return sb.toString();
    }

    /**
     * 获取今天日期
     * @param str 格式化字符
     * @return
     */
    public static String getToDay(String str){
        DateFormat dateFormat = new SimpleDateFormat(str);
        String date = dateFormat.format(new Date());
        return date;
    }
}
