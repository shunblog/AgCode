package entity;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/1.
 */
public class Property implements Serializable{

    private String key;

    private String value;

    public Property(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
