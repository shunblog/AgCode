package entity;

import utils.CamelCaseUtils;

import java.io.Serializable;

/**
 * Created by liushun on 2016/11/18.
 */
public class Column implements Serializable {

    private String columnName; //字段名称

    private String sqlColumnName; //原始字段带注解

    private String sqlColumn; //原始数据字段名称

    private String columnType; //字段类型

    private String columnDescription; //字段描述

    private String columnCapital; //首字母大写

    public Column(String columnName, String columnType, String columnDescription){
        this.columnName = columnName;
        this.sqlColumn = columnName;
        this.columnCapital = CamelCaseUtils.frist2Capitals(CamelCaseUtils.toCamelCase(columnName));
        this.sqlColumnName = "@Column(name = \""+columnName+"\")\n    ";
        String javaType = CamelCaseUtils.sqlType2JavaType(columnType);
        this.columnType = (javaType==null?"":javaType);
        this.columnDescription = columnDescription;
    }

    public String getColumnName() {
        return CamelCaseUtils.toCamelCase(columnName);
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSqlColumn() {
        return sqlColumn;
    }

    public void setSqlColumn(String sqlColumn) {
        this.sqlColumn = sqlColumn;
    }

    public String getSqlColumnName() {
        return sqlColumnName;
    }

    public void setSqlColumnName(String sqlColumnName) {
        this.sqlColumnName = sqlColumnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnDescription() {
        return columnDescription;
    }

    public void setColumnDescription(String columnDescription) {
        this.columnDescription = columnDescription;
    }

    public String getColumnCapital() {
        return columnCapital;
    }

    public void setColumnCapital(String columnCapital) {
        this.columnCapital = columnCapital;
    }
}
