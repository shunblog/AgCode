package entity;

import java.io.Serializable;

/**
 * Created by liushun on 2016/11/18.
 */
public class Table implements Serializable {

    private String tableName; //表名称

    private Integer tableLines; //记录数

    private String tableDescription; //表描述

    public Table(String tableName, Integer tableLines, String tableDescription){
        this.tableName = tableName;
        this.tableLines = tableLines;
        this.tableDescription = tableDescription;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Integer getTableLines() {
        return tableLines;
    }

    public void setTableLines(Integer tableLines) {
        this.tableLines = tableLines;
    }

    public String getTableDescription() {
        return tableDescription;
    }

    public void setTableDescription(String tableDescription) {
        this.tableDescription = tableDescription;
    }
}
