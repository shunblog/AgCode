package entity;

import java.io.Serializable;

/**
 * 数据库连接信息
 * Created by liushun on 2016/11/22.
 */
public class DbConnect implements Serializable{

    private String driver; // 驱动程序名

    private String url;  // URL指向要访问

    private String user;  // 数据库用户名

    private String password; //数据库密码

    private Integer dbType; //数据库类型 0、Mysql 1、Oracle 2、SQL Server 3、PostgreSQL

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDbType() {
        return dbType;
    }

    public void setDbType(Integer dbType) {
        this.dbType = dbType;
    }
}
