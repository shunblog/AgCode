package ui.tab;

import ui.AgCodeMain;
import utils.FileUtil;

import javax.swing.*;
import java.awt.*;
import java.io.*;

/**
 * AgCode生成器说明页面
 * Created by liushun on 2016/11/21.
 */
public class ReadMeTab extends JPanel {

    public ReadMeTab(){
        JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setEditable(false);
        jEditorPane.setContentType("text/html");
        jEditorPane.setBackground(new Color(238, 243, 230));
        try {
            InputStream is = this.getClass().getResourceAsStream("/readme.html");
            jEditorPane.setText(FileUtil.readInputStream(is));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JScrollPane jScrollPane = new JScrollPane(jEditorPane);
        jScrollPane.setBounds(0, 0 ,388, 280);
        this.add(jScrollPane);
        this.setLayout(null);
    }
}
