package ui.tab;

import entity.DbConnect;
import org.jvnet.substance.skin.SubstanceCremeCoffeeLookAndFeel;
import ui.AgCodeMain;
import utils.DBUtil;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据源配置面板
 * Created by liushun on 2016/11/21.
 */
public class DataSourceConfigTab extends JPanel{

    private JComboBox dataType;

    private JLabel sjlx, ipdz, dk, sjk, yhm, mima, ljzt, status;

    private JTextField fwqip_txt, dk_txt, sjk_txt, yhm_txt;

    private JPasswordField mima_txt;

    private JButton cslj;

    //获取数据名称
    public String getSjkTxt(){
        return sjk_txt.getText();
    }

    public DataSourceConfigTab(){
        sjlx = new JLabel("数据库类型：");
        sjlx.setBounds(50, 28, 100, 30);
        this.add(sjlx);

        dataType = new JComboBox(new String[]{"MySQL", "Oracle", "SQL Server","PostgreSQL"});
        dataType.setEnabled(false);
        dataType.setBounds(130, 30, 200, 23);
        this.add(dataType);


        ipdz = new JLabel("服务器IP地址：");
        ipdz.setBounds(39, 60, 100, 30);
        this.add(ipdz);

        fwqip_txt = new JTextField(15);
        fwqip_txt.setToolTipText("请设置服务器Ip地址");
        fwqip_txt.setText(AgCodeMain.config.getProperty("db.ip"));
        fwqip_txt.setBounds(130, 65, 200, 23);
        this.add(fwqip_txt);

        dk = new JLabel("端口：");
        dk.setBounds(85, 96, 100, 30);
        this.add(dk);

        dk_txt = new JTextField(10);
        dk_txt.setBounds(130, 100, 70, 23);
        dk_txt.setText(AgCodeMain.config.getProperty("db.port"));
        this.add(dk_txt);

        sjk = new JLabel("数据库：");
        sjk.setBounds(73, 132, 100, 30);
        this.add(sjk);

        sjk_txt = new JTextField(15);
        sjk_txt.setBounds(130, 135, 150, 23);
        sjk_txt.setToolTipText("请输入数据库名称");
        sjk_txt.setText(AgCodeMain.config.getProperty("db.name"));
        this.add(sjk_txt);

        yhm = new JLabel("用户名：");
        yhm.setBounds(73, 168, 100, 30);
        this.add(yhm);

        yhm_txt = new JTextField(15);
        yhm_txt.setBounds(130, 170, 200, 23);
        yhm_txt.setText(AgCodeMain.config.getProperty("db.username"));
        yhm_txt.setToolTipText("请输入用户名");
        this.add(yhm_txt);

        mima = new JLabel("密码：");
        mima.setToolTipText("请输入密码");
        mima.setBounds(85, 201, 100, 30);
        this.add(mima);

        mima_txt = new JPasswordField();
        mima_txt.setBounds(130, 205, 200, 23);
        mima_txt.setText(AgCodeMain.config.getProperty("db.password"));
        this.add(mima_txt);

        cslj = new JButton("测数据库连接");
        cslj.setBounds(50, 250, 100, 25);
        //按钮被点击
        cslj.addActionListener(new DataBaseActionListener());
        this.add(cslj);

        ljzt = new JLabel("连接状态：");
        ljzt.setBounds(200, 250, 60, 30);
        this.add(ljzt);

        status = new JLabel("未连接");
        status.setBounds(260, 250, 100, 30);
        this.add(status);
        this.setLayout(null);
    }

    public void setConfig(){
        AgCodeMain.config.setProperty("db.ip", fwqip_txt.getText());
        AgCodeMain.config.setProperty("db.name", sjk_txt.getText());
        AgCodeMain.config.setProperty("db.port", dk_txt.getText());
        AgCodeMain.config.setProperty("db.username", yhm_txt.getText());
        AgCodeMain.config.setProperty("db.password", String.valueOf(mima_txt.getPassword()));
    }

    // 测试数据按钮的监听事件
    private class DataBaseActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            SwingUtilities.invokeLater(new Runnable(){
                @Override
                public void run() {
                    status.setText("正在连接中....");
                    status.setForeground(Color.black);
                    DbConnect connect = new DbConnect();
                    connect.setDbType(dataType.getSelectedIndex());
                    connect.setUrl("jdbc:mysql://"+fwqip_txt.getText()+":"+dk_txt.getText()+"/"+sjk_txt.getText());
                    connect.setUser(yhm_txt.getText());
                    connect.setPassword(String.valueOf(mima_txt.getPassword()));
                    try {
                        DBUtil _db = DBUtil.getInstance(connect);
                        AgCodeMain.log(sjk_txt.getText()+" 数据库连接成功!", Color.blue, true);
                        status.setText("已成功建立连接!");
                        status.setForeground(new Color(0, 128, 0));
                        JOptionPane.showMessageDialog(null, "数据库连接成功!", "提示",JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception e) {
                        AgCodeMain.log(e.getMessage(), Color.red, true);
                        status.setText("连接失败!");
                        status.setForeground(Color.red);
                        JOptionPane.showMessageDialog(null, "数据库连接失败!", "提示",JOptionPane.ERROR_MESSAGE);
                    }finally {
                        Thread.yield();
                    }
                }
            });
        }
    }

}
