package ui.tab;

import ui.AgCodeMain;

import javax.swing.*;
import java.awt.*;

/**
 * Created by liushun on 2016/11/23.
 */
public class ColumnConverTab extends JPanel {

    private JLabel ts, str, intg, bln, date, tstime, clob, blob, bigdec, flt, dbe;

    private JTextField str_txt, intg_txt, bln_txt, date_txt, tstime_txt, clob_txt, blob_txt, bigdec_txt, flt_txt, dbe_txt;

    public ColumnConverTab(){

        JPanel panel = new JPanel();
        ts = new JLabel("注意：Java常用的类型，对应数据库字段类型用“|”分隔开");
        ts.setBounds(38, 0, 350, 30);
        ts.setForeground(Color.red);
        panel.add(ts);

        str = new JLabel("String 类型：");
        str.setBounds(38, 28, 100, 30);
        panel.add(str);

        str_txt = new JTextField(15);
        str_txt.setBounds(120, 32, 230, 23);
        str_txt.setText(AgCodeMain.config.getProperty("conver.String"));
        panel.add(str_txt);

        intg = new JLabel("Integer 类型：");
        intg.setBounds(30, 62, 100, 30);
        panel.add(intg);

        intg_txt = new JTextField(15);
        intg_txt.setBounds(120, 65, 230, 23);
        intg_txt.setText(AgCodeMain.config.getProperty("conver.Integer"));
        panel.add(intg_txt);

        bln = new JLabel("Boolean 类型：");
        bln.setBounds(23, 92, 100, 30);
        panel.add(bln);

        bln_txt = new JTextField(15);
        bln_txt.setBounds(120, 95, 230, 23);
        bln_txt.setText(AgCodeMain.config.getProperty("conver.Boolean"));
        panel.add(bln_txt);

        date = new JLabel("Date 类型：");
        date.setBounds(45, 122, 100, 30);
        panel.add(date);

        date_txt = new JTextField(15);
        date_txt.setBounds(120, 125, 230, 23);
        date_txt.setText(AgCodeMain.config.getProperty("conver.Date"));
        panel.add(date_txt);

        tstime = new JLabel("Timestamp 类型：");
        tstime.setBounds(8, 152, 120, 30);
        panel.add(tstime);

        tstime_txt = new JTextField(15);
        tstime_txt.setBounds(120, 155, 230, 23);
        tstime_txt.setText(AgCodeMain.config.getProperty("conver.Timestamp"));
        panel.add(tstime_txt);

        clob = new JLabel("Clob 类型：");
        clob.setBounds(45, 182, 100, 30);
        panel.add(clob);

        clob_txt = new JTextField(15);
        clob_txt.setBounds(120, 185, 230, 23);
        clob_txt.setText(AgCodeMain.config.getProperty("conver.Clob"));
        panel.add(clob_txt);

        blob = new JLabel("Blob 类型：");
        blob.setBounds(45, 212, 100, 30);
        panel.add(blob);

        blob_txt = new JTextField(15);
        blob_txt.setBounds(120, 215, 230, 23);
        blob_txt.setText(AgCodeMain.config.getProperty("conver.Blob"));
        panel.add(blob_txt);

        bigdec = new JLabel("BigDecimal 类型：");
        bigdec.setBounds(8, 242, 120, 30);
        panel.add(bigdec);

        bigdec_txt = new JTextField(15);
        bigdec_txt.setBounds(120, 245, 230, 23);
        bigdec_txt.setText(AgCodeMain.config.getProperty("conver.BigDecimal"));
        panel.add(bigdec_txt);

        flt = new JLabel("Float 类型：");
        flt.setBounds(8, 272, 120, 30);
        panel.add(flt);

        flt_txt = new JTextField(15);
        flt_txt.setBounds(120, 275, 230, 23);
        flt_txt.setText(AgCodeMain.config.getProperty("conver.Float"));
        panel.add(flt_txt);

        dbe = new JLabel("Double 类型：");
        dbe.setBounds(8, 302, 120, 30);
        panel.add(dbe);

        dbe_txt = new JTextField(15);
        dbe_txt.setBounds(120, 305, 230, 23);
        dbe_txt.setText(AgCodeMain.config.getProperty("conver.Double"));
        panel.add(dbe_txt);

        panel.setPreferredSize(new Dimension(350, 350));
        panel.setLayout(null);
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setBounds(0, 0 ,388, 280);
        JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
        verticalScrollBar.setPreferredSize(new Dimension(12, 0));
        this.add(scrollPane);
        this.setLayout(null);
    }
}
